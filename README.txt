"Egmont Overture Finale" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Mourning Song" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Junkyard Tribe" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

Press Q & E to rotate player.
A & D to move
Click on enemies when clone ability is ready to store them.
press X when you have enough scrap to make a friendly copy of that enemy

You win when the north wall is destroyed.
You lose when the south wall is destroyed.